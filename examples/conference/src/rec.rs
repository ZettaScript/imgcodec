use crate::util::*;

use rand::Rng;
use std::io::Write;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct Opt {
	#[structopt(default_value = "video.syeve")]
	file: String,
	#[structopt(default_value = "10", long)]
	fps: f64,
}

fn bgra_to_rgb(bgra: &[u8], rgb: &mut [u8]) {
	bgra.chunks_exact(4)
		.zip(rgb.chunks_exact_mut(3))
		.for_each(|(bgra, rgb)| {
			rgb[0] = bgra[2];
			rgb[1] = bgra[1];
			rgb[2] = bgra[0];
		});
}

pub async fn run(opt: Opt) {
	let mut file = std::fs::File::create(opt.file).unwrap();
	let mut capturer = scrap::Capturer::new(scrap::Display::primary().unwrap()).unwrap();

	let size = (capturer.width(), capturer.height());
	println!("Image size: {:?}", size);

	let mut encoder = syeve::Encoder::new(
		size,
		3,
		1800,
	);
	encoder.set_seq_number(rand::thread_rng().gen());

	let mut buf_frame = valloc(size.0 * size.1 * 3);
	let mut buf_packet = Vec::new();

	let mut interval =
		async_timer::Interval::platform_new(core::time::Duration::from_secs_f64(1.0 / opt.fps));

	loop {
		if let Ok(image) = capturer.frame() {
			bgra_to_rgb(&image, &mut buf_frame);
			encoder.encode(&mut buf_frame, &mut buf_packet).unwrap();
			file.write_all(&(buf_packet.len() as u32).to_be_bytes())
				.unwrap();
			file.write_all(&buf_packet).unwrap();
			buf_packet.clear();
		}
		interval.as_mut().await;
	}
}
