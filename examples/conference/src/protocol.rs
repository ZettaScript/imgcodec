/// Request sent by the receiver of a stream to its sender
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ReverseRequest {
	/// Request to start a new sequence
	NewSequence = 1,
}
